/*
 * Copyright 2018-2020 YuRaNnNzZZ
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ru.ffgs.compat.trbaubles;

import baubles.common.Baubles;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import ru.ffgs.compat.trbaubles.events.ClientEventHandler;
import ru.ffgs.compat.trbaubles.events.RegisterItemEventHandler;
import techreborn.lib.ModInfo;

@Mod(
		modid = TRBaubles.MOD_ID,
		name = TRBaubles.MOD_NAME,
		version = TRBaubles.MOD_VERSION,
		dependencies = "required-after:" + Baubles.MODID + "; required-after:" + ModInfo.MOD_ID + ";"
)
public class TRBaubles {
	public static final String MOD_ID = "trbaubles";
	public static final String MOD_NAME = "TechReborn Baubles";
	public static final String MOD_VERSION = "@VERSION@";

	@Mod.EventHandler
	public void preInit(FMLPreInitializationEvent event) {
		MinecraftForge.EVENT_BUS.register(new RegisterItemEventHandler());
	}
	
	@SideOnly(Side.CLIENT)
	@Mod.EventHandler
	public void clientInit(FMLInitializationEvent event) {
		MinecraftForge.EVENT_BUS.register(new ClientEventHandler());
	}
}
