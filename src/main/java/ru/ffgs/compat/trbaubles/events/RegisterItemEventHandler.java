/*
 * Copyright 2018-2020 YuRaNnNzZZ
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ru.ffgs.compat.trbaubles.events;

import net.minecraft.item.Item;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import ru.ffgs.compat.trbaubles.items.ItemBaubleCloakingDevice;
import ru.ffgs.compat.trbaubles.items.ItemBaubleLapotronPack;
import ru.ffgs.compat.trbaubles.items.ItemBaubleLithiumBatpack;
import techreborn.init.ModItems;
import techreborn.lib.ModInfo;

public class RegisterItemEventHandler {
	@SubscribeEvent
	public void registerModItems(RegistryEvent.Register<Item> event) {
		System.out.println(ModItems.LAPOTRONIC_ORB_PACK);
		ModItems.LAPOTRONIC_ORB_PACK = new ItemBaubleLapotronPack();
		ModItems.registerItem(ModItems.LAPOTRONIC_ORB_PACK, ModInfo.MOD_ID + ":lapotronPack");
		
		System.out.println(ModItems.LITHIUM_BATTERY_PACK);
		ModItems.LITHIUM_BATTERY_PACK = new ItemBaubleLithiumBatpack();
		ModItems.registerItem(ModItems.LITHIUM_BATTERY_PACK, ModInfo.MOD_ID + ":lithiumBatpack");
		
		System.out.println(ModItems.CLOAKING_DEVICE);
		ModItems.CLOAKING_DEVICE = new ItemBaubleCloakingDevice();
		ModItems.registerItem(ModItems.CLOAKING_DEVICE, ModInfo.MOD_ID + ":cloakingdevice");
	}
	
}
