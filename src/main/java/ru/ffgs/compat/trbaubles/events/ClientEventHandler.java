/*
 * Copyright 2018-2020 YuRaNnNzZZ
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ru.ffgs.compat.trbaubles.events;

import baubles.api.BaubleType;
import baubles.api.BaublesApi;
import baubles.api.cap.IBaublesItemHandler;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraftforge.client.event.RenderPlayerEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import techreborn.init.ModItems;

@SideOnly(Side.CLIENT)
public class ClientEventHandler {
	private int baubleSlotBody = BaubleType.BODY.ordinal();

	@SubscribeEvent
	public void renderPlayer(RenderPlayerEvent.Pre event) {
		EntityPlayer player = event.getEntityPlayer();

		IBaublesItemHandler baublesItemHandler = BaublesApi.getBaublesHandler(player);
		if (baublesItemHandler == null) return;

		ItemStack bodyBaubleStack = baublesItemHandler.getStackInSlot(baubleSlotBody);

		if (!bodyBaubleStack.isEmpty() && bodyBaubleStack.getItem() == ModItems.CLOAKING_DEVICE && player.isInvisible()) {
			event.setCanceled(true);
		}
	}
}
