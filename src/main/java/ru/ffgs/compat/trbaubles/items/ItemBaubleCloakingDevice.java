/*
 * Copyright 2018-2020 YuRaNnNzZZ
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ru.ffgs.compat.trbaubles.items;

import baubles.api.BaubleType;
import baubles.api.IBauble;
import baubles.api.render.IRenderBauble;
import net.minecraft.client.Minecraft;
import net.minecraft.client.model.ModelBiped;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.MobEffects;
import net.minecraft.inventory.EntityEquipmentSlot;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import reborncore.common.registration.RebornRegistry;
import techreborn.items.armor.ItemCloakingDevice;
import techreborn.lib.ModInfo;

@RebornRegistry(modID = ModInfo.MOD_ID)
public class ItemBaubleCloakingDevice extends ItemCloakingDevice implements IBauble, IRenderBauble {
	@Override
	public BaubleType getBaubleType(ItemStack itemstack) {
		return BaubleType.BODY;
	}

	@Override
	public void onWornTick(ItemStack itemstack, EntityLivingBase player) {
		this.onArmorTick(player.getEntityWorld(), (EntityPlayer) player, itemstack);
	}

	@Override
	public void onEquipped(ItemStack itemstack, EntityLivingBase player) {

	}

	@Override
	public void onUnequipped(ItemStack itemstack, EntityLivingBase player) {
		if (player.isInvisible() && !player.isPotionActive(MobEffects.INVISIBILITY)) {
			player.setInvisible(false);
		}
	}

	@Override
	public boolean canEquip(ItemStack itemstack, EntityLivingBase player) {
		return true;
	}

	@Override
	public boolean canUnequip(ItemStack itemstack, EntityLivingBase player) {
		return true;
	}

	@Override
	public boolean willAutoSync(ItemStack itemstack, EntityLivingBase player) {
		return true;
	}

	@SideOnly(Side.CLIENT)
	private ResourceLocation armorTexture;

	@SideOnly(Side.CLIENT)
	private ModelBiped model;

	@SideOnly(Side.CLIENT)
	@Override
	public void onPlayerBaubleRender(ItemStack stack, EntityPlayer player, RenderType type, float partialTicks) {
		if (type != RenderType.BODY || player.isInvisible()) return;

		if (armorTexture == null) {
			armorTexture = new ResourceLocation(getArmorTexture(stack, player, EntityEquipmentSlot.CHEST, null));
		}

		if (model == null) {
			model = new ModelBiped(1F);
		}

		Minecraft.getMinecraft().renderEngine.bindTexture(armorTexture);
		Helper.rotateIfSneaking(player);

		model.bipedBody.render(0.0625F);
		model.bipedLeftArm.render(0.0625F);
		model.bipedRightArm.render(0.0625F);
	}
}
